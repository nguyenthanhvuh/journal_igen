\documentclass{sig-alternate-05-2015}

\usepackage[T1]{fontenc}
\usepackage[table]{xcolor}
\usepackage[utf8]{inputenc} 
\usepackage[vlined,figure,linesnumbered]{algorithm2e}
\usepackage{amsmath}
\usepackage{balance}
\usepackage{booktabs}
\usepackage{cite}
\usepackage{natbib}
\usepackage{color}
\usepackage{hyperref}
\usepackage{microtype} 
\usepackage{pdflscape}
\usepackage{pgfplots}
\usepackage{siunitx}

\usepackage{dcolumn}
\newcolumntype{d}[1]{D{.}{.}{#1}}
\newcommand\mc[1]{\multicolumn{1}{c}{#1}} % handy shortcut macro


\pgfplotsset{compat=newest}

\definecolor{dkgreen}{rgb}{0,0.5,0}
\definecolor{dkred}{rgb}{0.5,0,0}
\definecolor{dkgray}{rgb}{0.3,0.3,0.3}
\usepackage{tikz}
\usetikzlibrary{arrows,shadows,positioning,shapes}

\makeatletter
\providecommand*{\cupdot}{%
  \mathbin{%
    \mathpalette\@cupdot{}%
  }%
}

\newcommand*{\@cupdot}[2]{%
  \ooalign{%
    $\m@th#1\sqcup$\cr
    \hidewidth$\m@th#1\cdot$\hidewidth
  }%
}
\makeatother

\usepackage{listings}
\lstset{basicstyle=\sffamily,
  language=Python,
  keywordstyle=\color{blue},
  ndkeywordstyle=\color{red},
  commentstyle=\color{dkgreen},
  stringstyle=\color{dkred},
  numbers=left,
  numberstyle=\ttfamily\footnotesize\color{dkgray},
  stepnumber=1,
  numbersep=8pt,
  backgroundcolor=\color{white},
  tabsize=2,
  showspaces=false,
  showstringspaces=false,
  emph={try,catch}, emphstyle=\color{red}\bfseries,
  breaklines,
  breakatwhitespace,
  mathescape,
  literate={"}{{\ttfamily"}}1
  {<-}{$\leftarrow$}2
  {!=}{$\neq$}1
  {punion}{$\cupdot$}1,
  columns=flexible,
  morekeywords={then,end,do},
}

\newcommand{\lt}[1]{{\lstinline@#1@}} 
\newcommand{\mycomment}[3][\color{red}]{{#1{[{#2}: {#3}]}}}
\newcommand{\jeff}[1]{\mycomment[\color{red}]{JSF}{#1}}
\newcommand{\adam}[1]{\mycomment[\color{orange}]{AAP}{#1}}
\newcommand{\tvn}[1]{\mycomment[\color{blue}]{TVN}{#1}}
\newcommand{\ugur}[1]{\mycomment[\color{brown}]{UK}{#1}}
\newcommand{\jav}[1]{\mycomment[\color{green}]{JC}{#1}}
\newcommand{\paul}[1]{\mycomment[\color{purple}]{PG}{#1}}
\newcommand{\aset}[1]{\{#1\}}
\newcommand{\punion}{\cupdot}
\newcommand{\hdrc}[1]{\multicolumn{1}{c}{\cellcolor{black!30}{\textsf{#1}}}}
\newcommand{\hdrr}[1]{\multicolumn{1}{r}{\cellcolor{black!30}{\textsf{#1}}}}
\newcommand{\hdrl}[1]{\multicolumn{1}{l}{\cellcolor{black!30}{\textsf{#1}}}}
\newcommand{\hdrv}[1]{\cellcolor{black!30}{\textsf{#1}}}
\newcommand{\hdrt}[1]{\textsf{#1}}
\newcommand{\pfmt}[1]{\textsf{#1}}
\newcommand{\itree}{iTree} 

\newboolean{long}
\font\xtiny=cmr10 scaled 600
\definecolor{siqrcolor}{gray}{0}

\def\tableentryraw#1#2{\num{#1}\parbox{16pt}{\raggedleft\color{siqrcolor}\xtiny#2\normalcolor}}
\def\tableentrysingle#1{\num{#1}\parbox{16pt}}

\def\mso#1#2{
	\ifx\\#1\\%
		-
	\else%
		\ifthenelse{\equal{#1}{0}}{\tableentryraw{#1}{#2}}{
			\ifthenelse{\boolean{long}}{\tableentryraw{#1}{#2}}{#1}}
	\fi%
}

\def\c#1#2{\multicolumn{#1}{c|}{#2}}
\def\cc#1{\multicolumn{1}{c|}{#1}}

\graphicspath{{./figures/}}

\begin{document}
\toappear{}
%\setcopyright{acmcopyright}

\title{iGen: Dynamic Interaction Inference for Configurable Software}

\numberofauthors{1}
\author{
%  \alignauthor ThanhVu Nguyen \hspace*{0.8em} Ugur Koc  \hspace*{0.8em}
%   Jeffrey S. Foster  \hspace*{0.8em} Adam A. Porter
%  \affaddr{University of Maryland, College Park, USA}\\
%  \email{\{tnguyen, ukoc, javran, jfoster, aporter\}@cs.umd.edu}
}

\maketitle

\begin{abstract}

\end{abstract}

\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10011007.10011074.10011099.10011102.10011103</concept_id>
<concept_desc>Software and its engineering~Software testing and debugging</concept_desc>
<concept_significance>500</concept_significance>
</concept>

<concept>
<concept_id>10011007.10011006.10011071</concept_id>
<concept_desc>Software and its engineering~Software configuration management and version control systems</concept_desc>
<concept_significance>500</concept_significance>
</concept>

<concept>
<concept_id>10011007.10010940.10010992.10010998.10011001</concept_id>
<concept_desc>Software and its engineering~Dynamic analysis</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Software and its engineering~Software testing and debugging}
\ccsdesc[500]{Software and its engineering~Software configuration management and version control systems}
\ccsdesc[500]{Software and its engineering~Dynamic analysis}
%\printccsdesc

%\keywords{Program analysis;  software testing; configurable systems; dynamic analysis}

\section{Experiments}

In these experiments we used iGen~\cite{nguyen2016igen} to automatically find the precise interactions leading to the variability bugs studied by Abal et al. \cite{abal201442}.
Note that these bugs do not exist in all configurations or executions.
Programs should be configured in certain ways so that such bugs manifest themselves.
Figure~\ref{examplebug} shows an example variability bug from the variability database\footnote{http://vbdb.itu.dk}. 
In this example, a `null pointer deference' problem occurs only when the \textit{ENABLE\_SELINUX} option is set and \textit{ENABLE\_FEATURE\_ST\\AT\_FORMAT} option is unset.
Both of these options are compile-time configuration options.
In these experiments, we used the simplified bug snippets for benchmarks \textit{Linux, Busybox, and Marlin}.


Tables~\ref{linuxexp}-\ref{marlinexp} presents the findings of iGen. We can summarize these findings as follows in three cases;

\begin{itemize}
    \item in cases like unused variable and dead code, we could not come up with an easy mechanism to make them detectable automatically. Therefore for these cases, iGen could not find the interactions leading to the reported problems.
    \item in all cases where the simplified code snippets were accurately mimicking the variability bug for the original code, iGen was able to find the precise interaction leading to these bugs.
    \item in many cases, the simplified bug snippet could not accurately mimic the behavior of the original code. For these cases, we modified the the simplified code snippets to make them accurately mimic the behavior of the original code and also to make the bug easier to detect. Then, iGen was able to find the precise interactions for these cases too.
\end{itemize}

None of the reported interactions were too complicated for iGen.

\subsection{Configuring iGen}

iGen uses an interestingness script which outputs the interesting results from evaluation of the benchmark program. 
In~\cite{nguyen2016igen}, we designed interestingness scripts to output line coverage information.
Hence, iGen gave us configuration option interactions that are necessary to satisfy to reach specific lines of a program.
In these experiments, our goal is to find ithe specific configuration settings that lead to the variability bugs~\cite{abal201442}.
Therefore, we designed the interestingness scripts to detect and output these bugs.
For the example program in Figure~\ref{examplebug}, the interestingness script would output the error message for the null pointer dereference problem.


For this study, we are not interested in coverage information. 
Instead, we are interested in compiler warnings, compilation errors, and runtime errors. 
Therefore, the interesting scripts in these experiments only outputs these kinds results from compilation and test execution.

\begin{figure}[t]
 \begin{lstlisting}[numbers=none,xleftmargin=1em,language=C]
void do_stat(const char *filename) {
#ifdef ENABLE_SELINUX
  char *scontext = NULL;
#endif

#ifndef ENABLE_FEATURE_STAT_FORMAT
#ifdef ENABLE_SELINUX
  printf(" %lc\n", *scontext); // ERROR
#endif
  printf("  File: '%s'\n", filename);
#endif
}

void main(int argc, char **argv) {
  do_stat("filename");
}
\end{lstlisting}
\caption{An example variability bug from the variability database (simplified for presentation)}\label{examplebug}
\label{fig:examplebug}
\end{figure} 



\begin{table*}
\caption{Bechmarks.}\label{benchmarks}
\centering
\begin{tabular}{l|l|l}
\hdrv{benchmark} & \hdrc{opts} & \hdrc{\# of bugs}\\
linux & 91 binary options & 40 \\
busybox & 34 binary options & 16 \\
busybox & 908 binary options & 16 \\
marlin & 20 options & 13\\
\end{tabular}
\end{table*}


\begin{table*}
\caption{iGen experiments summary.}\label{benchmarks}
\centering
\begin{tabular}{l|l|l}
\hdrv{benchmark} & \hdrc{\# of bugs}\\
linux    &    40    & \\
busybox    &    16 \\
marlin    &    13\\
\end{tabular}
\end{table*}

\begin{landscape}
\begin{table*}
\scriptsize
\caption{Linux experiments with modified simplified bug snippets.} \label{linuxexp2}
\hspace{-8cm}
\begin{tabular}{l|l|l|l|l}
\hdrv{} & \hdrv{} & \hdrv{found} & \hdrc{} & \hdrv{}\\
\hdrv{bug} & \hdrv{} & \hdrv{right} & \hdrv{} & \hdrv{}\\
\hdrv{category} & \hdrv{snippet} & \hdrv{inter.} &\hdrv{modification} & \hdrv{explanation}\\
assertion violation (CWE 617) & \href{http://vbdb.itu.dk/#bug/linux/0988c4c}{0988c4c} & yes & NA & \\
assertion violation (CWE 617) & \href{http://vbdb.itu.dk/#bug/linux/208d898}{208d898} & YES? & assertion added & added body to a function-like macro \\
assertion violation (CWE 617) & \href{http://vbdb.itu.dk/#bug/linux/63878ac}{63878ac} & yes & NA & \\
assertion violation (CWE 617) & \href{http://vbdb.itu.dk/#bug/linux/657e964}{657e964} & yes & NA & \\
assertion violation (CWE 617) & \href{http://vbdb.itu.dk/#bug/linux/ae249b5}{ae249b5} & YES! & none & DISCONTIGMEM $\wedge$ PROC\_PAGE\_MONITOR igen was right for the given snippet \\
assertion violation (CWE 617) & \href{http://vbdb.itu.dk/#bug/linux/d549f55}{d549f55} & yes & NA & \\
assertion violation (CWE 617) & \href{http://vbdb.itu.dk/#bug/linux/eb91f1d}{eb91f1d} & YES & fixed snippet &  \\
attempt to use a resource after having freed it (CWE 416) & \href{http://vbdb.itu.dk/#bug/linux/6e2b757}{6e2b757} & YES? & pointer set to NULL & after first free \\
attempt to write on read-only resource & \href{http://vbdb.itu.dk/#bug/linux/221ac32}{221ac32} & YES & fixed the snippet & it was not accurate based on the reported interaction \\
buffer overflow (CWE 120) & \href{http://vbdb.itu.dk/#bug/linux/60e233a}{60e233a} & YES & assertion added\\
buffer overflow (CWE 120) & \href{http://vbdb.itu.dk/#bug/linux/8c82962}{8c82962} & YES & assertion added \\
buffer overflow (CWE 120) & \href{http://vbdb.itu.dk/#bug/linux/f3d83e2}{f3d83e2} & YES & assertion added & poor snippet \\
dead code (CWE 561) & \href{http://vbdb.itu.dk/#bug/linux/809e660}{809e660} & no & excluded & no observable output \\
double acquiring lock (CWE 764) & \href{http://vbdb.itu.dk/#bug/linux/d7e9711}{d7e9711} & YES? & assertion added & mutex type changed to ERRORCHECK and dyn initialization \\
double operation on resource (CWE 675) & \href{http://vbdb.itu.dk/#bug/linux/0dc77b6}{0dc77b6} & YES! & exit with nonzero & ANDROID $\wedge$ EXTCON and igen is right for the given script\\
double operation on resource (CWE 675) & \href{http://vbdb.itu.dk/#bug/linux/472a474}{472a474} & YES! & none & SMP iGen was right for the given snippet\\
expected behavior violation & \href{http://vbdb.itu.dk/#bug/linux/1f758a4}{1f758a4} & YES! & exit with nonzero & EP93XX\_ETH need more look into\\
fail to release memory before removing last reference to it (CWE 401) & \href{http://vbdb.itu.dk/#bug/linux/218ad12}{218ad12} & no & none & no observable output, memory leak \\
function is defined multiple times & \href{http://vbdb.itu.dk/#bug/linux/e68bb91}{e68bb91} & yes & NA & \\
incompatible types (CWE 843) & \href{http://vbdb.itu.dk/#bug/linux/d6c7e11}{d6c7e11} & YES? & added printf & which attempts printing the value pointed be the double pointer \\
incompatible types (CWE 843) & \href{http://vbdb.itu.dk/#bug/linux/e1fbd92}{e1fbd92} & YES & assertion added & poor snippet \\
null pointer dereference (CWE 476) & \href{http://vbdb.itu.dk/#bug/linux/6252547}{6252547} & yes & NA & \\
null pointer dereference (CWE 476) & \href{http://vbdb.itu.dk/#bug/linux/76baeeb}{76baeeb} & YES! & none & NUMA $\wedge$ PCI $\wedge$ !X86\_64, igen was right for the given snippet \\
null pointer dereference (CWE 476) & \href{http://vbdb.itu.dk/#bug/linux/ee3f34e}{ee3f34e} & yes & NA & \\
null pointer dereference (CWE 476) & \href{http://vbdb.itu.dk/#bug/linux/f7ab9b4}{f7ab9b4} & yes & NA & \\
numeric truncation (CWE 197) & \href{http://vbdb.itu.dk/#bug/linux/51fd36f}{51fd36f} & YES? & assertion added & time value should be positive \\
read out of the intended bounds (CWE 125) & \href{http://vbdb.itu.dk/#bug/linux/0f8f809}{0f8f809} & YES! & assertion added & (LOCKDEP $\wedge$ !SLOB) $\wedge$ (PPC\_256K\_PAGES | PPC\_64K\_PAGES | !SLAB) igen was right for the given snippet \\ & & & & PLUS (LOCKDEP $\wedge$ SLOB) results in CTR ‘kmalloc\_caches’ undeclared \\
read out of the intended bounds (CWE 125) & \href{http://vbdb.itu.dk/#bug/linux/91ea820}{91ea820} & YES! & assertion added & DISCONTIGMEM $\wedge$ PROC\_PAGE\_MONITOR igen is right for the given snippet\\
read out of the intended bounds (CWE 125) & \href{http://vbdb.itu.dk/#bug/linux/c708c57}{c708c57} & YES! & assertion added & S390\_PRNG igen is right for the given snippet, the snippet is still missing one config option \\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/linux/6651791}{6651791} & YES! &  none & (SND\_FSI\_AK4642 | SND\_FSI\_DA7210 | SND\_SOC\_AK4642 | SND\_SOC\_DA7210) $\wedge$ !I2C\\ & & & & igen was right for the given snippet\\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/linux/f48ec1d}{f48ec1d} & yes & NA & \\
undefined symbol/function reference & \href{http://vbdb.itu.dk/#bug/linux/242f1a3}{242f1a3} & yes & NA & \\
undefined symbol/function reference & \href{http://vbdb.itu.dk/#bug/linux/2f02c15}{2f02c15} & yes & NA & \\
undefined symbol/function reference & \href{http://vbdb.itu.dk/#bug/linux/6515e48}{6515e48} & yes & NA & \\
undefined symbol/function reference & \href{http://vbdb.itu.dk/#bug/linux/7c6048b}{7c6048b} & YES! & none & ACPI\_VIDEO $\wedge$ !BACKLIGHT\_CLASS\_DEVICE, igen was right for the given snippet \\
unused variable (CWE 563) & \href{http://vbdb.itu.dk/#bug/linux/36855dc}{36855dc} & no & excluded & no observable output \\
use of variable before initialization (CWE 457) & \href{http://vbdb.itu.dk/#bug/linux/1c17e4d}{1c17e4d} & no & excluded & no observable output \\
use of variable before initialization (CWE 457) & \href{http://vbdb.itu.dk/#bug/linux/30e0532}{30e0532} & no & excluded & no observable output \\
use of variable before initialization (CWE 457) & \href{http://vbdb.itu.dk/#bug/linux/7acf6cd}{7acf6cd} & no & excluded & no observable output \\
use of variable before initialization (CWE 457) & \href{http://vbdb.itu.dk/#bug/linux/e39363a}{e39363a} & no & excluded & no observable output \\
use of variable before initialization (CWE 457) & \href{http://vbdb.itu.dk/#bug/linux/bc8cec0}{bc8cec0} & yes & NA & \\
warning: dereferencing `void *' pointer & \href{http://vbdb.itu.dk/#bug/linux/d530db0}{d530db0} & yes & NA & \\
wrong number of arguments in function application (CWE 685) & \href{http://vbdb.itu.dk/#bug/linux/e67bc51}{e67bc51} & yes & NA & \\
\end{tabular}
\end{table*}
\end{landscape}

\newpage
\begin{landscape}
\begin{table*}
\scriptsize
\caption{Busybox experiments with simplified bug snippets.}\label{busyboxexp}
\hspace{-8cm}
\begin{tabular}{l|l|l|l|l}
\hdrv{} & \hdrv{} & \hdrv{found} & \hdrc{} & \hdrv{}\\
\hdrv{bug} & \hdrv{} & \hdrv{right} & \hdrv{} & \hdrv{}\\
\hdrv{category} & \hdrv{snippet} & \hdrv{inter.} &\hdrv{modification} & \hdrv{explanation}\\
attempt to use a resource after having freed it (CWE 416) & \href{http://vbdb.itu.dk/#bug/busybox/bc0ffc0}{bc0ffc0} & yes & & exists in version 1\_20 which compiles\\
expected behavior violation & \href{http://vbdb.itu.dk/#bug/busybox/9575518}{9575518} & YES? & other problems fixed & this script was not compiling due to four bugs none of which is \\
&  &  & and assertion added & the reported one (exist in version 1\_17 which does not compile)\\
expected behavior violation & \href{http://vbdb.itu.dk/#bug/busybox/df7b657}{df7b657} & yes &  & exist in version 1\_17 which does not compile \\
fail to release memory before removing last reference to it (CWE 401) & \href{http://vbdb.itu.dk/#bug/busybox/2631486}{2631486} & YES? & other problems fixed & never compiles there is another bug (exists in version 1\_00 does not compile) \\
&  & & and assertion added & (request for member ‘val’ in something not a structure or union) \\
incompatible types (CWE 843) & \href{http://vbdb.itu.dk/#bug/busybox/5cd6461}{5cd6461} & YES? & assertion added & randomness removed \\
null pointer dereference (CWE 476) & \href{http://vbdb.itu.dk/#bug/busybox/199501f}{199501f} & YES? & strcat dest src swapped & randomness \\
null pointer dereference (CWE 476) & \href{http://vbdb.itu.dk/#bug/busybox/1b487ea}{1b487ea} & yes & increased chances & randomness (exist in version 1\_22 which compiles) \\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/busybox/5275b1e}{5275b1e} & yes & \\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/busybox/b7ebc61}{b7ebc61} & yes & \\
undefined symbol/function reference & \href{http://vbdb.itu.dk/#bug/busybox/061fd0a}{061fd0a} & no & excluded & script asks for user input\\
undefined symbol/function reference & \href{http://vbdb.itu.dk/#bug/busybox/cf1f2ac}{cf1f2ac} & yes &  & exists in version 1\_00 does not compile \\
undefined symbol/function reference & \href{http://vbdb.itu.dk/#bug/busybox/ebee301}{ebee301} & yes &  \\
unused variable (CWE 563) & \href{http://vbdb.itu.dk/#bug/busybox/0301ffa}{0301ffa} & no & excluded & not observable \\
unused variable (CWE 563) & \href{http://vbdb.itu.dk/#bug/busybox/192c35f}{192c35f} & no & excluded & not observable \\
unused variable (CWE 563) & \href{http://vbdb.itu.dk/#bug/busybox/b62bd7b}{b62bd7b} & no & excluded & not observable \\
use of variable before initialization (CWE 457) & \href{http://vbdb.itu.dk/#bug/busybox/b273d66}{b273d66} & no & excluded & not observable \\
use of variable before initialization (CWE 457) & \href{http://vbdb.itu.dk/#bug/busybox/eef2317}{eef2317} & no & excluded & segfaults (not really) \\
\end{tabular}
\end{table*}
\end{landscape}

\newpage
\begin{landscape}
\begin{table*}
\scriptsize
\caption{Marlin experiments with simplified bug snippets.}\label{marlinexp}
\hspace{-8cm}
\begin{tabular}{l|l|l|l|l}
\hdrv{} & \hdrv{} & \hdrv{found} & \hdrc{} & \hdrv{}\\
\hdrv{bug} & \hdrv{} & \hdrv{right} & \hdrv{} & \hdrv{}\\
\hdrv{category} & \hdrv{snippet} & \hdrv{inter.} &\hdrv{modification} & \hdrv{explanation}\\
expected behavior violation & \href{http://vbdb.itu.dk/#bug/marlin/2d22902}{2d22902} & yes & & \\
expected behavior violation & \href{http://vbdb.itu.dk/#bug/marlin/53be0f3}{53be0f3} & no & & does not compile, includes header files which do not exist \\
incompatible types (CWE 843) & \href{http://vbdb.itu.dk/#bug/marlin/2db384a}{2db384a} & no & added includes & int var is passed to a unsigned long function parameter, does not really cause compilation problem \\
incompatible types (CWE 843) & \href{http://vbdb.itu.dk/#bug/marlin/fc3c76f}{fc3c76f} & yes! & & but the bug manifests in a different way \\
integer overflow (CWE 190) & \href{http://vbdb.itu.dk/#bug/marlin/3024821}{3024821} & yes & & \\
integer overflow (CWE 190) & \href{http://vbdb.itu.dk/#bug/marlin/31873ec}{31873ec} & no & macros modified & includes added, int overflow happens due to bitwise operations \\
Stack based buffer overflow (CWE 121) & \href{http://vbdb.itu.dk/#bug/marlin/a7fc1f8}{a7fc1f8} & no & & there are other problems \\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/marlin/b8e79dc}{b8e79dc} & yes & & \\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/marlin/7336e6d}{7336e6d}  & yes & & there are other problems too \\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/marlin/e30bfed}{e30bfed} & no? & removed define macro & snippet was defining an config option regardless \\
undeclared identifier/macro/variable & \href{http://vbdb.itu.dk/#bug/marlin/fdac8f6}{fdac8f6} & yes & & \\
wrong number of arguments in function application (CWE 685) & \href{http://vbdb.itu.dk/#bug/marlin/831016b}{831016b}  & yes & & another problem in the snippet \#endif without \#if \\
wrong number of arguments in function application (CWE 685) & \href{http://vbdb.itu.dk/#bug/marlin/8c4377d}{8c4377d} & yes & & there are other problems too \\
\end{tabular}
\end{table*}
\end{landscape}

Note that we get the exactly same results running iGen with entire config space.

\subsection{A case study of Busybox; Increasing number of configuration options}\label{increasing}

Figure~\ref{timeVSopts} shows iGen's execution time for increasing number of configuration options.

\begin{table*}
\centering
\caption{Busybox increasing configuration options experiments}\label{increasingtbl}
\begin{tabular}{r|r|r|r|r|r|r}
\hdrv{opts} & \hdrv{iter} & \hdrv{inters} & \hdrc{t\_time} & \hdrc{x\_time} & \hdrv{configs} & \hdrv{findings} \\
20	&	4.0	(0.5)	&	3.0	(0.0)	&	1839.218 (0426.176)	&	0.547 (000.056)	&	36.0 (6.5)	&	5.0 (0.0) \\
40	&	8.0 (0.0)	&	5.0 (0.0)	&	4933.251 (0156.382)    &	2.314 (000.142)	&	102.0 (0.0)	&	7.0 (0.0)\\
80	&	7.0 (2.0)	&	4.0 (0.0)	&	10890.722 (1074.079)	&	9.132 (000.792)	&	250.0 (0.5)	&	8.0 (0.0)\\
160	&	34.0 (10.5)	&	11.0 (1.5)	&	38557.351 (6548.671)	&	217.542 (047.018)	&	1791.0 (281.5)	&	15.0	 (1.0)\\
320	&	39.0	 (3.0) &	14.0 (1.5) &	89539.947 (4311.069)	&	1773.154 (290.409)	&	4780.0 (421.5)	&	17.0	 (1.0) \\
640	&	53 (0.0)	&	18.0	 (0.0)	&	278422.585 (0000.000)	&	18243.005 (000.000)	&	11469.0 (0.0) &	24.0 (0.0) \\
\hline
\end{tabular}
\end{table*}
\begin{figure}
 \centering
 \includegraphics[width=0.5\columnwidth, angle=-90]{bb_time.eps}
 \caption{iGen execution time}\label{timeVSopts}
\end{figure}





\input{related}

\balance
\bibliographystyle{abbrv}
\bibliography{config}  
\end{document}

