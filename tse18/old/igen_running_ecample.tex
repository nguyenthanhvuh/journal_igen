\documentclass{sig-alternate-05-2015}

\usepackage[utf8]{inputenc} 
\usepackage[T1]{fontenc}
\usepackage{microtype} 

\usepackage{balance}
\usepackage{cite}
\usepackage[vlined,figure,linesnumbered]{algorithm2e}
\usepackage{booktabs}
\usepackage{color}
\usepackage{amsmath}
\usepackage[table]{xcolor}
\usepackage{siunitx}
\usepackage{hyperref}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

\definecolor{dkgreen}{rgb}{0,0.5,0}
\definecolor{dkred}{rgb}{0.5,0,0}
\definecolor{dkgray}{rgb}{0.3,0.3,0.3}
\usepackage{tikz}
\usetikzlibrary{arrows,shadows,positioning,shapes}

\makeatletter
\providecommand*{\cupdot}{%
  \mathbin{%
    \mathpalette\@cupdot{}%
  }%
}

\newcommand*{\@cupdot}[2]{%
  \ooalign{%
    $\m@th#1\sqcup$\cr
    \hidewidth$\m@th#1\cdot$\hidewidth
  }%
}
\makeatother

\usepackage{listings}
\lstset{basicstyle=\sffamily,
  language=Python,
  keywordstyle=\color{blue},
  ndkeywordstyle=\color{red},
  commentstyle=\color{dkgreen},
  stringstyle=\color{dkred},
  numbers=left,
  numberstyle=\ttfamily\footnotesize\color{dkgray},
  stepnumber=1,
  numbersep=8pt,
  backgroundcolor=\color{white},
  tabsize=2,
  showspaces=false,
  showstringspaces=false,
  emph={try,catch}, emphstyle=\color{red}\bfseries,
  breaklines,
  breakatwhitespace,
  mathescape,
  literate={"}{{\ttfamily"}}1
  {<-}{$\leftarrow$}2
  {!=}{$\neq$}1
  {punion}{$\cupdot$}1,
  columns=flexible,
  morekeywords={then,end,do},
}

\newcommand{\lt}[1]{{\lstinline@#1@}} 
\newcommand{\mycomment}[3][\color{red}]{{#1{[{#2}: {#3}]}}}
\newcommand{\jeff}[1]{\mycomment[\color{red}]{JSF}{#1}}
\newcommand{\adam}[1]{\mycomment[\color{orange}]{AAP}{#1}}
\newcommand{\tvn}[1]{\mycomment[\color{blue}]{TVN}{#1}}
\newcommand{\ugur}[1]{\mycomment[\color{brown}]{UK}{#1}}
\newcommand{\jav}[1]{\mycomment[\color{green}]{JC}{#1}}
\newcommand{\aset}[1]{\{#1\}}
\newcommand{\punion}{\cupdot}
\newcommand{\hdrc}[1]{\multicolumn{1}{c}{\cellcolor{black!30}{\textsf{#1}}}}
\newcommand{\hdrr}[1]{\multicolumn{1}{r}{\cellcolor{black!30}{\textsf{#1}}}}
\newcommand{\hdrl}[1]{\multicolumn{1}{l}{\cellcolor{black!30}{\textsf{#1}}}}
\newcommand{\hdrv}[1]{\cellcolor{black!30}{\textsf{#1}}}
\newcommand{\hdrt}[1]{\textsf{#1}}
\newcommand{\pfmt}[1]{\textsf{#1}}
\newcommand{\itree}{iTree} 

\newboolean{long}
\font\xtiny=cmr10 scaled 600
\definecolor{siqrcolor}{gray}{0}

\def\tableentryraw#1#2{\num{#1}\parbox{16pt}{\raggedleft\color{siqrcolor}\xtiny#2\normalcolor}}
\def\tableentrysingle#1{\num{#1}\parbox{16pt}}

\def\mso#1#2{
	\ifx\\#1\\%
		-
	\else%
		\ifthenelse{\equal{#1}{0}}{\tableentryraw{#1}{#2}}{
			\ifthenelse{\boolean{long}}{\tableentryraw{#1}{#2}}{#1}}
	\fi%
}

\def\c#1#2{\multicolumn{#1}{c|}{#2}}
\def\cc#1{\multicolumn{1}{c|}{#1}}

\begin{document}
\toappear{}
%\setcopyright{acmcopyright}

\title{iGen+: Dynamic Interaction Inference for Configurable Software}

\numberofauthors{1}
\author{}

\begin{figure}[t]
 \begin{lstlisting}[numbers=none,xleftmargin=1em,language=C]
// options: $s$, $t$, $u$, $v$, $x$, $y$, $z$
int max_z = 3;

if($x$ && $y$) {
  printf("$L0$\n"); // $x \wedge y$
  if (!(0 < $z$ && $z$ < max_z)){
    printf("$L1$\n"); // $x \wedge y \wedge (z\in\aset{0, 3, 4})$
  }
}else{
  printf("$L2$\n"); // $\neg{x} \vee \neg{y}$
}
printf("$L3$\n");  // $\textit{true}$
if($u$ && $v$) {
  printf("$L4$\n");  // $u \wedge v $
  if($s$ || $t$) {
    printf("$L5$\n");  // $u \wedge v \wedge (s \vee t) $
  }
}
\end{lstlisting}
\caption{Program with seven configuration options. 
Locations L0--L5 are annotated with associated interactions.}
\label{fig1}
\end{figure} 

We next use the C program in Figure~\ref{fig1} to explain the details of iGen.
This program has seven configuration options, listed on the first line of the figure. 
The first six options are boolean-valued, and the last one, $z$, ranges over the set $\{0,1,2,3,4\}$.
Thus, this program has $2^6 \times 5 = 320$ possible configurations.

For this example, iGen initializes \textsf{configs} to the following covering array:
\[
\footnotesize
\begin{array}{c|rrrrrrr|l}
\text{config}& s & t & u & v & x & y & z & \text{coverage}\\
\midrule
c_1 & 0 & 0 & 1 & 1 & 1 & 0 & 1 & L2,L3,L4 \\
c_2 & 1 & 1 & 0 & 0 & 1 & 1 & 0 & L0,L1,L3\\
c_3 & 0 & 0 & 1 & 1 & 0 & 0 & 2 & L2,L3,L4 \\
c_4 & 0 & 0 & 1 & 1 & 1 & 1 & 3 & L0,L1,L3,L4 \\
c_5 & 0 & 1 & 1 & 1 & 1 & 0 & 4 & L2,L3,L4,L5  \\
\end{array}
\]
We list the coverage of each configuration on the right. Let us look at what we know about L5 so far.

\[
\footnotesize
\begin{array}{r|rrrrrrr}
L5 & s & t & u & v & x & y & z \\
\midrule
c_5   & 0 & 1 & 1 & 1 & 1 & 0 & 4\\
\midrule
\textrm{union} & 0 & 1 & 1 & 1 & 1 & 0 & 4 \\
\end{array}
\]

\[
\footnotesize
\begin{array}{r|rrrrrrr}
L5' & s & t & u & v & x & y & z \\
\midrule
c_1 & 0 & 0 & 1 & 1 & 1 & 0 & 1 \\
c_2 & 1 & 1 & 0 & 0 & 1 & 1 & 0 \\
c_3 & 0 & 0 & 1 & 1 & 0 & 0 & 2 \\
c_4 & 0 & 0 & 1 & 1 & 1 & 1 & 3 \\
\midrule
\textrm{union} & \top & \top & \top & \top & \top & \top & 0,1,2,3 \\
\textrm{neg} & F & F & F & F & F & F & 4 \\
\end{array}
\]

So for L5 $conj= \neg s \wedge t \wedge u \wedge v \wedge x \wedge \neg y \wedge z=4,  disj = z=4 $. At this point, for L5 iGen will generate following counter examples for the conjunction interaction.

\[
\footnotesize
\begin{array}{c|rrrrrrr|l}
\text{config}& s & t & u & v & x & y & z & \text{coverage}\\
\midrule
c_6   & 1 & 1 & 1 & 1 & 1 & 0 & 4 & L5 \\
c_7   & 0 & 0 & 1 & 1 & 1 & 0 & 4 & \\
c_8   & 0 & 1 & 0 & 1 & 1 & 0 & 4 & \\
c_9   & 0 & 1 & 1 & 0 & 1 & 0 & 4 & \\
c_{10}   & 0 & 1 & 1 & 1 & 0 & 0 & 4 & L5 \\
c_{11}   & 0 & 1 & 1 & 1 & 1 & 1 & 4 & L5 \\
c_{12}   & 0 & 1 & 1 & 1 & 1 & 0 & 3 & L5 \\
\midrule
\textrm{union} for L5 & \top & \top & 1 & 1 & \top & \top & 3,4 \\
\end{array}
\]

Here each configuration disagrees with $\textsf{conj} = \neg s \wedge t \wedge u \wedge v \wedge x \wedge \neg y \wedge z=4$ in one setting.

Then the next iteration of the fix-point loop will compute
$\textsf{conj}$ for $L5$ from $c_6$, $c_{10}$, $c_{11}$, and $c_{12}$. Which is 
$\textsf{conj} =  u \wedge v \wedge z={3,4}$.

Instead of doing this, we change the half of the options that appear in the interaction to a different value. E.g.

\[
\footnotesize
\begin{array}{c|rrrrrrr|l}
\text{config}& s & t & u & v & x & y & z & \text{coverage}\\
\midrule
c_{13}   & 1 & 0 & 0 & 0 & 1 & 0 & 4 & \\
c_{14}   & 0 & 1 & 1 & 1 & 0 & 1 & 3 & L5 \\
c_5   & 0 & 1 & 1 & 1 & 1 & 0 & 4 & L5 \\
\midrule
\textrm{union} for L5 & 0 & 1 & 1 & 1 & \top & \top & 3,4 \\
\end{array}
\]

\paragraph*{Disjunctive Interactions}
Next let us consider the interaction $\neg{x} ~\vee~ \neg{y}$ for $L2$
in Figure~\ref{fig1}. By construction, $\textsf{conj}$ cannot encode
this formula---although the membership constraints in $\textsf{conj}$
are a form of disjunction (e.g., $z\in\aset{0,3}$ is the same as
$z=0 \vee z=3$), they cannot represent disjunctions among different
variables.

There are a variety of potential ways to infer more general
disjunctions, but we want to maintain the same efficiency as inferring
conjunctive interactions. To motivate iGen's approach to disjunctions,
observe that $L2$'s interaction arises because an else branch was
taken. In fact, $L2$'s interaction is exactly the negation of the
interaction for $L0$ from the true branch. Thus,
iGen computes disjunctive interactions by first computing a non-covering
interaction, which is a \emph{conjunctive} interaction
for the configurations that do \emph{not} cover line $L2$,
and then \emph{negates} it to get a disjunctive
interaction for $L2$ (line~\ref{line:disj}).
In our running example, $c_2$ and $c_4$ are the only
configurations that do not cover $L2$, thus iGen computes 
$c_2\punion c_4 = x\wedge y\wedge (z\in\aset{0,3})$. 
Negating that yields $\textsf{disj} =\neg{x} \vee \neg{y} ~\vee~ (z\in\aset{1,2,4})$, which is 
close to the correct interaction for $L2$.

Notice this approach to
disjunctions is a straightforward extension of conjunctive interaction
inference. Also notice that it is heuristic since the computed
interaction may not actually cover the given line; thus disjunctive interactions
may be eliminated on line~\ref{line:check} of the algorithm in Figure~\ref{alg:igen}.

Disjunctive interactions can be refined in two ways. First, they may be
refined by coincidence if \textsf{genNewConfigs} selects a long
conjunctive interaction to refine. 
Second, \textsf{genNewConfigs} also considers the negation of
$\textsf{disj}$ as a possible longest interaction to use for
refinement (essentially refining an interaction describing configurations
that do not reach the current location).

\paragraph*{Mixed Interactions}
Finally, some interactions require mixtures of conjunctions and
disjunctions, such as the interaction $u\wedge v \wedge (s\vee t)$ for
$L5$. Looking at Figure~\ref{fig1}, notice this interaction occurs
because a disjunctive condition is nested inside of a conjunctive
condition---in fact, the interaction for $L5$ is the interaction for
$L4$ with one additional clause.

This motivates iGen's approach to inferring mixed interactions by
\emph{extending} shorter
interactions. Lines~\ref{line:conjmstart}--\ref{line:conjmend} give
the code for computing $\textsf{conjdisj}$.  Recall that to compute
the pure disjunction $\textsf{disj}$, iGen negates the pointwise
union of non-covering configurations. On line~\ref{line:conjmstart} we
use the same idea to compute $\textsf{disj}'$, but instead of
\emph{all} non-covering configurations, we only include the
non-covering configurations that satisfy $\textsf{conj}$.
Essentially we are projecting the iGen algorithm onto just
configurations that satisfy that interaction. Thus, when we infer the
disjunction $\textsf{disj}'$, we conjoin it onto $\textsf{conj}$ to
compute the final mixed interaction.

For our running example, after several iterations $\textsf{conj}$
for $L5$ will be $u\wedge v$ (details not shown). Out of the configurations
that do not cover $L5$, only $c_1$, $c_3$, and $c_4$ also
satisfy $u\wedge v$. Thus $\textsf{disj}'$ will be
$\neg(c_1\punion c_3\punion c_4) = s \vee \ t \vee \neg u \vee \neg v
\vee (z\in\aset{0,4})$.
Thus after some simplification we get
$\textsf{conj} \wedge \textsf{disj}' = u \wedge v \wedge (s
\vee t \vee (z\in\aset{0,4}))$,
which is almost the interaction for $L5$. 
After further iteration, iGen eventually reaches the final, fully precise
interaction for $L5$.

Using the dual of the above approach,
lines~\ref{line:disjmstart}--\ref{line:disjmend} infer another
mixed interaction $\textsf{disjconj}$ by extending the computed
disjunctive interaction $\textsf{disj}$ with a conjunction
$\textsf{conj}'$. Here $\textsf{conj}'$ is generated just like
$\textsf{conj}$, but we only include configurations that disagree in
some setting with some clause of $\textsf{disj}$, since otherwise the configuration is already
included in the left side of the disjunct on line~\ref{line:disjmend}.


Notice that iGen's approach for inferring mixed interactions maintains
the efficiency of computing pure conjunctions and disjunctions. We
could extend the algorithm further to compute conjunctions with nested
disjunctions with nested conjunctions etc., but we have not explored
that yet. 

Lastly, in addition to considering $\textsf{conj}$ and $\textsf{disj}$
as potential longest interactions, \textsf{genNewConfigs} also
considers $\textsf{conj'}$; and (negated) $\textsf{disj'}$. Thus,
mixed interactions may be refined whenever one of those four
components is refined.

\balance
\bibliographystyle{abbrv}
\bibliography{vu_bibs,vu_bibs1}  
\end{document}

