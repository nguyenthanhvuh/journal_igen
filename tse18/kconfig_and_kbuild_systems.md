# Kconfig/Kbuild Systems

- Linux <https://www.kernel.org/>

- BusyBox <https://busybox.net/>

- Toybox - an embedded toolkit started by the BusyBox
  maintainer. <http://www.landley.net/toybox/>

- coreboot - open-source BIOS firmware  <https://www.coreboot.org/>

- uClibc/uClibc-ng (next generation) - libc for embedded systems
  <https://uclibc-ng.org/>

- uClibc++ - libc++ for embedded systems <https://cxx.uclibc.org/>

- uClinux - port of Linux for systems without an MMU <http://www.uclinux.org/>

- Fiasco - implementation of the L4 microkernel <https://os.inf.tu-dresden.de/fiasco/>

- axTLS - embedded SSL library with HTTP client/server <http://axtls.sourceforge.net/>

- EmbToolkit - toolchain builder for embedded systems <https://www.embtoolkit.org/>
